# devops-netology
Hello! It`s my first commit on GitHub :)

[.gitignore] в дирректории

**/.terraform/* - исключает папку тераформ

*.tfstate - игнор файлов с любым именем и расширением *.tfstate

crash.log - не замечаем конкретно этот файл

*.tfvars - в игнор любой файл с расширением - *.tfvars

override.tf
override.tf.json
*_override.tf
*_override.tf.json  - определяет игнорировать файлы override.tf / override.tf.json / [любое_имя]_override.tf / [любое_имя]_override.tf.json


.terraformrc
terraform.rc   - игнор конфигов Терраформ
